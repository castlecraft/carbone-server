import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService, ConvertTo, Payload } from './app.service';
import { Response } from 'express';

export const payload: Payload = {
  fileName: 'result.pdf',
  templateData: {
    firstname: 'John',
    lastname: 'Doe',
  },
  convertTo: ConvertTo.PDF,
  template: 'simple.odt',
};

const mockedResponse = {
  set: jest.fn(),
  end: jest.fn(),
  send: jest.fn(),
} as unknown as Response;

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "result.pdf!"', () => {
      expect(appController.getPdf(payload, mockedResponse)).toBeDefined();
    });
  });
});
