import {
  Body,
  Controller,
  Post,
  Res,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Response } from 'express';
import { AppService, Payload } from './app.service';
import { MimeTypes } from './mime-types';

@Controller('')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('download')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async getPdf(@Body() payload: Payload, @Res() response: Response) {
    const file: unknown & { length?: number } = await this.appService.getPdf(
      payload,
      payload.convertTo,
    );
    response.set({
      'Content-Type': `application/${MimeTypes[payload.convertTo]}`,
      'Content-Disposition': `attachment; filename=${payload?.fileName}`,
      'Content-Length': file.length,
    });
    return response.send(file);
  }
}
