import { Injectable, InternalServerErrorException } from '@nestjs/common';
import * as carbone from 'carbone';
import {
  IsEnum,
  IsNotEmpty,
  IsNotEmptyObject,
  IsString,
} from 'class-validator';

export enum ConvertTo {
  PDF = 'pdf',
  DOCX = 'docx',
  ODT = 'odt',
  ODS = 'ods',
  XLSX = 'xlsx',
  HTML = 'html',
  PPTX = 'pptx',
  ODP = 'odp',
}

export class Payload {
  @IsNotEmpty()
  @IsString()
  fileName: string;

  @IsNotEmpty()
  @IsString()
  template: string;

  @IsNotEmpty()
  @IsNotEmptyObject()
  templateData: any;

  @IsNotEmpty()
  @IsEnum(ConvertTo)
  convertTo: ConvertTo;
}

@Injectable()
export class AppService {
  async getPdf(payload: Payload, convertTo: ConvertTo = ConvertTo.PDF) {
    const templatePath =
      process.env.TEMPLATE_PATH || './node_modules/carbone/examples';
    const templateFile = templatePath + '/' + payload.template;
    return await new Promise((resolve, reject) => {
      carbone.render(
        templateFile,
        payload.templateData,
        { convertTo },
        (err, result) => {
          if (err) {
            reject(new InternalServerErrorException(err));
          }
          resolve(result);
        },
      );
    });
  }
}
