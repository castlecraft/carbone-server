FROM node:slim

RUN sed -i -e's/ main/ main contrib non-free/g' /etc/apt/sources.list && \
  apt-get update && \
  apt-get install --no-install-recommends -y \
  # For LibreOffice
  libxinerama1 \
  libfontconfig1 \
  libdbus-glib-1-2 \
  libcairo2 \
  libcups2 \
  libglu1-mesa \
  libsm6 \
  libreoffice \
  # For MS Fonts
  ttf-mscorefonts-installer && \
  # For downloading debs
  rm -rf /var/lib/apt/lists/*

COPY --chown=node:node . /opt/carbone-server

WORKDIR /opt/carbone-server

USER node

RUN yarn && yarn build && yarn install --production=true

CMD ["yarn", "start:prod"]
