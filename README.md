## Description

Application

## Installation

Local setup

```bash
yarn
```

## Running the app

```bash
# development
yarn start

# watch mode
yarn start:debug

# production mode
yarn start:prod
```

## Test

```bash
# unit tests
yarn test

# e2e tests
yarn test:e2e

# test coverage
yarn test:cov
```
## License

MIT.

